# mysqlapp

### Aplikasi Mengenai Peminjaman Sederhana pada Perpustakaan

### Route API 
#### Borrow
* ['GET'] /borrow
* route untuk mendapatkan semua buku yang dipinjam

* ['POST'] /borrow/insert
* route untuk meminjam buku berdasarkan bookid

* ['POST'] /borrow/status
* route untuk mengubah status buku yang dipinjam

#### Customer
* ['GET'] /users
* route untuk mendapatkan semua user

* ['GET'] /user/
* route untuk membuka user berdasarkan userid

* ['POST'] /user/insert
* route untuk menambahkan user

* ['POST'] /user/update
* route untuk mengupdate user

* ['POST'] /user/delete
* route untuk menghapus user

* ['GET'] /user/requesttoken
* route untuk mendapatkan token

