from app import app
from app.controllers import customer
from flask import Blueprint, request

customers_blueprint = Blueprint("customer", __name__)

@app.route('/users', methods=["GET"])
def showUsers():
    return customer.shows()

@app.route('/user', methods=["GET"])
def showUser():
    params = request.json
    return customer.show(**params)

@app.route('/user/insert', methods=["POST"])
def insertUser():
    params = request.json
    return customer.insert(**params)

@app.route('/user/update', methods=["POST"])
def updateUser():
    params = request.json
    return customer.update(**params)

@app.route('/user/delete', methods=["POST"])
def deleteUser():
    params = request.json
    return customer.delete(**params)

@app.route('/user/requesttoken', methods=["GET"])
def requestToken():
    params = request.json
    return customer.token(**params)