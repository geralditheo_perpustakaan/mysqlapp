from app import app
from app.controllers import borrow
from flask import Blueprint, request

borrow_blueprint = Blueprint("borrow", __name__)

@app.route('/borrows', methods=["GET"])
def showBorrow():    
    return borrow.shows()

@app.route('/borrows/insert', methods=["POST"])
def insertBorrow():
    params = request.json
    return borrow.insert(**params)
    

@app.route('/borrows/status', methods=["POST"])
def updateStatus():
    params = request.json
    return borrow.changeStatus(**params)