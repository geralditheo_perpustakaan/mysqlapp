from app.models.customer import database
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime

mysqldb = database()

def shows():
    dbresult = mysqldb.showUsers()
    result = []

    for item in dbresult:
        user = {
            "userid": item[0],
            "username": item[1],
            "firstname": item[2],
            "lastname": item[3],
            "email": item[4]
        }

        result.append(user)
    
    return jsonify(result)

def show(**params):

    dbresult = mysqldb.showUserById(**params)
    
    user = {
        "userid": dbresult[0],
        "username": dbresult[1],
        "firstname": dbresult[2],
        "lastname": dbresult[3],
        "email": dbresult[4]
    }

    return jsonify(user)

def insert(**params):
    deresult = mysqldb.insertUser(**params)
    mysqldb.dataCommit()

    return jsonify({
        "message": "Insert Success"
    })

def update(**params):
    deresult = mysqldb.updateUserById(**params)
    mysqldb.dataCommit()

    return jsonify({
        "message": "Update Success"
    })

def delete(**params):
    deresult = mysqldb.deleteUserById(**params)
    mysqldb.dataCommit()

    return jsonify({
        "message": "Delete Success"
    })

def token(**params):
    debresult = mysqldb.showUserByEmail(**params)
    if debresult is not None:
        # Payload untuk JWT
        user = {
            "username": debresult[1],
            "email": debresult[4]
        }

        expires = datetime.timedelta(days=1)
        access_token = create_access_token(user, fresh=True, expires_delta = expires)

        data = {
            "data": user,
            "token_access": access_token,
        }
    else:
        data = {
            "message": "Email tidak terdaftar"
        }

    return jsonify(data)