from mysql.connector import connect

class database:
    def __init__(self):
        try:
            self.db = connect(
                host = 'localhost',
                user = 'geraldi',
                password = 'geraldi',
                database = 'perpustakaan'
            )
        except Exception as e:
            print(e)

    def showBorrowByEmail(self, **params):
        

        query = " SELECT customers.username, borrows.* FROM borrows INNER JOIN customers ON borrows.userid = customers.userid where customers.email = '{0}' and borrows.isactive = 1 ".format(params['email'])

        cursor = self.db.cursor()
        cursor.execute(query)
        result = cursor.fetchall()

        return result

    def insertBorrow(self, **params):
        column = ', '.join(list(params.keys()))
        value = tuple(list(params.values()))

        query = " INSERT INTO borrows ({0}) VALUES {1} ".format(column, value)
        print(query)

        cursor = self.db.cursor()
        cursor.execute(query)


    def updateBorrow(self, **params):
        borrowid = params['borrowid']
        
        cursor = self.db.cursor()
        query = " UPDATE borrows set isactive = 0 WHERE borrowid = {0} ".format(borrowid)

        cursor.execute(query)

    def dataCommit(self):
        self.db.commit()

